# Evaluate chroma sharpness

Decide what's the best color resolution for encoding JPEG images. It evaluates sharpness of Cb and Cr channels and estimates whether subsampling would introduce visible artifacts.
