use rgb::RGB8;
use evalchroma::*;
use imgref::*;

#[track_caller]
fn img(path: &str) -> ImgVec<RGB8> {
    let img = lodepng::decode24_file(path).expect(path);
    imgref::ImgVec::new(img.buffer, img.width, img.height)
}

#[track_caller]
fn eval(img: ImgRef<RGB8>, q: f32) -> ChromaEvaluation {
    adjust_sampling(img, PixelSize { cb: (2,2), cr: (2,2) }, q)
}

#[test]
fn red() {
    let red = img("tests/scribble.png");
    let res = eval(red.as_ref(), 99.);
    assert!(res.chroma_quality > 91. && res.chroma_quality < 95., "{res:?}");
    assert_eq!(res.subsampling.cr, (1,1));
    assert_eq!(res.subsampling.cb, (1,1));

    let res = eval(red.as_ref(), 92.);
    assert_eq!(res.subsampling.cr, (1,1));
    assert_eq!(res.subsampling.cb, (1,1));

    let res = eval(red.as_ref(), 83.);
    assert_eq!(res.subsampling.cr, (1,1));
    assert_eq!(res.subsampling.cb, (2,1));

    let res = eval(red.as_ref(), 77.);
    assert!(res.chroma_quality > 70. && res.chroma_quality < 75., "{res:?}");
    assert_eq!(res.subsampling.cr, (1,1));
    assert_eq!(res.subsampling.cb, (2,2));

    let res = eval(red.as_ref(), 0.);
    assert_eq!(res.subsampling.cr, (1,1));
    assert_eq!(res.subsampling.cb, (2,2));
}

#[test]
fn grad() {
    let red = img("tests/grad.png");
    let res = eval(red.as_ref(), 99.);
    assert!(res.chroma_quality > 80. && res.chroma_quality < 93., "{res:?}");
    assert_eq!(res.subsampling.cr, (2,2));
    assert_eq!(res.subsampling.cb, (2,2));

    let res = eval(red.as_ref(), 40.);
    assert_eq!(res.subsampling.cr, (2,2));
    assert_eq!(res.subsampling.cb, (2,2));
}

#[test]
fn grayish() {
    let red = img("tests/grayish.png");
    let res = eval(red.as_ref(), 99.);
    assert!(res.chroma_quality > 69.8 && res.chroma_quality < 77., "{res:?}");
    assert_eq!(res.subsampling.cr, (2,2));
    assert_eq!(res.subsampling.cb, (2,2));

    let res = eval(red.as_ref(), 40.);
    assert_eq!(res.subsampling.cr, (2,2));
    assert_eq!(res.subsampling.cb, (2,2));
}

#[test]
fn bluew() {
    let red = img("tests/bluew.png");
    let res = eval(red.as_ref(), 75.);
    assert!(res.chroma_quality > 70. && res.chroma_quality < 77., "{res:?}");
    assert_eq!(res.subsampling.cr, (2,2));
    assert_eq!(res.subsampling.cb, (2,1));

    let res = eval(red.as_ref(), 50.);
    assert_eq!(res.subsampling.cr, (2,2));
    assert_eq!(res.subsampling.cb, (2,2));
}

#[test]
fn blueh() {
    let red = img("tests/blueh.png");
    let res = eval(red.as_ref(), 90.);
    assert!(res.chroma_quality > 80. && res.chroma_quality < 88., "{res:?}");
    assert_eq!(res.subsampling.cr, (2,2));
    assert_eq!(res.subsampling.cb, (1,2));

    let res = eval(red.as_ref(), 80.);
    assert_eq!(res.subsampling.cr, (2,2));
    assert_eq!(res.subsampling.cb, (1,2));

    let res = eval(red.as_ref(), 60.);
    assert_eq!(res.subsampling.cr, (2,2));
    assert_eq!(res.subsampling.cb, (2,2));
}

#[test]
fn gradw() {
    let red = img("tests/gradw.png");
    let res = eval(red.as_ref(), 90.);
    assert!(res.chroma_quality > 80. && res.chroma_quality < 88., "{res:?}");
    assert_eq!(res.subsampling.cr, (2,1));
    assert_eq!(res.subsampling.cb, (2,1));

    let res = eval(red.as_ref(), 70.);
    assert_eq!(res.subsampling.cr, (2,1));
    assert_eq!(res.subsampling.cb, (2,2));
}

#[test]
fn gradh() {
    let red = img("tests/gradh.png");
    let res = eval(red.as_ref(), 90.);
    assert!(res.chroma_quality > 80. && res.chroma_quality < 88., "{res:?}");
    assert_eq!(res.subsampling.cr, (1,2));
    assert_eq!(res.subsampling.cb, (1,2));

    let res = eval(red.as_ref(), 80.);
    assert_eq!(res.subsampling.cr, (1,2));
    assert_eq!(res.subsampling.cb, (2,2));
}

#[test]
fn worst() {
    let red = img("tests/worst.png");
    let res = eval(red.as_ref(), 90.);
    assert!(res.chroma_quality > 80. && res.chroma_quality < 88., "{res:?}");
    assert_eq!(res.subsampling.cr, (1,1));
    assert_eq!(res.subsampling.cb, (1,1));

    let res = eval(red.as_ref(), 80.);
    assert_eq!(res.subsampling.cr, (1,1));
    assert_eq!(res.subsampling.cb, (1,1));

    let res = eval(red.as_ref(), 40.);
    assert_eq!(res.subsampling.cr, (1,1));
    assert_eq!(res.subsampling.cb, (1,1));

    let res = eval(red.as_ref(), 20.);
    assert_eq!(res.subsampling.cr, (1,1));
    assert_eq!(res.subsampling.cb, (1,1));
}

#[test]
fn blur() {
    let red = img("tests/blur.png");
    let res = eval(red.as_ref(), 100.);
    assert!(res.chroma_quality > 91. && res.chroma_quality < 95., "{res:?}");
    assert_eq!(res.subsampling.cr, (1,1));
    assert_eq!(res.subsampling.cb, (2,2));

    let res = eval(red.as_ref(), 95.);
    assert!(res.chroma_quality > 85. && res.chroma_quality < 94., "{res:?}");
    assert_eq!(res.subsampling.cr, (2,2));
    assert_eq!(res.subsampling.cb, (2,2));

    let res = eval(red.as_ref(), 11.);
    assert!(res.chroma_quality > 7. && res.chroma_quality < 11., "{res:?}");
    assert_eq!(res.subsampling.cr, (2,2));
    assert_eq!(res.subsampling.cb, (2,2));
}
